const fs = require("fs");
const { GitLabAPI } = require("gitlab-api-async-iterator")();
const { DateTime } = require("luxon");
const {
  CURRENT_PROJECT,
  IS_DEV,
  NAVIGATION_UPDATE_TEMPLATE_PATH,
} = require("./constants");

function getNavigationUpdateDescription(dateString) {
  const descriptionTemplate = fs
    .readFileSync(NAVIGATION_UPDATE_TEMPLATE_PATH, "utf8")
    .replace("Mon DD, 2022", dateString);
  return descriptionTemplate;
}

function getNavigationUpdateTitle(dateString) {
  return `Navigation update: ${dateString}`;
}

async function maybeCreateNavigationUpdate() {
  const dateString = DateTime.utc().toFormat("DD");

  console.log(`Trying to create the issue for ${dateString}`);

  try {
    const payload = {
      title: getNavigationUpdateTitle(dateString),
      description: getNavigationUpdateDescription(dateString),
    };

    if (IS_DEV) {
      console.log(
        "[dev] skipped navigation update creation. Payload:",
        JSON.stringify(payload)
      );
      return;
    } else {
      const { data: created } = await GitLabAPI.post(
        `/projects/${CURRENT_PROJECT}/issues`,
        payload
      );
      console.log(
        `✅ Successfully created navigation update for ${dateString}`
      );
      console.log(created.web_url);
    }
  } catch (e) {
    console.log(`Could not create issue for ${dateString}`);
    console.log(e);
  }
}

async function main() {
  await maybeCreateNavigationUpdate();
}

main().catch((e) => {
  console.log("ERROR", e);
  process.exit(1);
});
